pragma solidity ^0.4.0;

import "github.com/oraclize/ethereum-api/oraclizeAPI.sol";


contract Software is usingOraclize{
    address owner;
    
    struct Task {
        string testRepo;
        uint reward;
        address requestor;
        bool submitted;
        bool accepted;
        string solutionRepo;
        address solver;
        bytes proof;
    }
    enum queryType { repoExists, repoSolves }
    struct oraclizeCallback {
    // for which purpose did we call? {ForUSD | ForDistance}
        queryType qType;
        uint taskId;
     }
    
    // Lookup state from queryIds
    mapping (bytes32 => oraclizeCallback) public oraclizeCallbacks;
    
    mapping (uint => Task) public tasks;
    uint numTasks;
    string public queryResult;
    
    function Software() public {
        owner = msg.sender;
        oraclize_setCustomGasPrice(4000000000 wei);
        oraclize_setProof(proofType_TLSNotary);
    }
    
    event taskAdded(uint id, string testRepo, uint reward);
    event taskSolved(uint taskId);
    event solutionSubmitted(uint taskId);
    event solutionRejected(uint taskId);

    event newOraclizeQuery(string description);

    	
    function addTask(string testRepo) public payable returns(uint){
        //TODO - check if the testRepo really exists
        tasks [++numTasks] = Task (testRepo, msg.value, msg.sender, false, false, "", 0x0,  "");
        taskAdded(numTasks, testRepo, msg.value);
        return numTasks;
    }
  
    
    function __callback(bytes32 queryId, string result, bytes proof) public {
        require (msg.sender == oraclize_cbAddress());
        queryResult = result; // let's save it as $ cents
        
        if(oraclizeCallbacks[queryId].qType == queryType.repoSolves){
            uint taskId = oraclizeCallbacks[queryId].taskId;
            
            if(keccak256(result) == keccak256("success")){
                tasks[taskId].submitted = true;
                tasks[taskId].proof = proof;
            }else{
                tasks[taskId].solver = 0x0;
                tasks[taskId].solutionRepo = "";
            }
        }
    }
    
    /* 
     * the requestor should verify the proof, the solution and accept it to unlock founds
     */
    function validateSolution(uint taskId) public {
        require(msg.sender == tasks[taskId].requestor);
        tasks[taskId].accepted = true;        
        tasks[taskId].solver.transfer(tasks[taskId].reward);
        taskSolved(taskId);
    }
    /*
     * the requestor can reject the solution if the proof verification failed
     */
    function rejectSolution(uint taskId) public {
        require(msg.sender == tasks[taskId].requestor);
        tasks[taskId].accepted = false;        
        tasks[taskId].solver = 0x0;
        tasks[taskId].solutionRepo = "";
        tasks[taskId].submitted = false;
        tasks[taskId].submitted = false;
    }
    
    function trySolution(uint taskId, string solutionRepo) public {
        
        //check if there's noone else who submitted a solution
        require(tasks[taskId].solver == 0x0);
        
        //TODO - check if test/configuration files are the same on both repositories
        
        //TODO - check if the build is passing the tests and set "working" variable accordingly
        newOraclizeQuery("Oraclize query was sent, standing by for the answer..");
        bytes32 queryId = oraclize_query('URL', "json(http://srene.ddns.jazztel.es:8080/test.json).status");
        tasks[taskId].solver = msg.sender;
        tasks[taskId].solutionRepo = solutionRepo;
        oraclizeCallbacks[queryId] = oraclizeCallback(queryType.repoSolves, taskId);
    }
    
    
}
