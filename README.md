## Repositories

* Repo without a solution - https://gitlab.com/mharnen/contract-software-test
* Repo with a solution - https://gitlab.com/oascigil/contract-software-test

## Links

* TLSNotary - https://tlsnotary.org/TLSNotary.pdf
* Oraclize -http://www.oraclize.it/